Archive of cryptocuttlefish (@cuttlefish_btc) tweets, in chronological order, numbered, in plaintext.

Data ripped by [redacted] in JSON form. 
Extremely quick & dirty cleanup for readability. Uploading here for prosperity's sake but also happy to flesh this out & compile contributions when possible.

TODO:
### overall ###
- scrape images
- hyperlinks
- thread

### text manipulation ###
- text formatting
    * remove inverted commas
    * spaces between EOL https links

### long-term ###
- organize threads?
